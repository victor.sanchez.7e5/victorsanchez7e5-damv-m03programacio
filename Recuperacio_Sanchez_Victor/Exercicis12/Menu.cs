/*
Autor : Víctor Sánchez Íñiguez
Data : 10-06-2022
*/

using System;

namespace Exercicis12
{
    public class Menu
    {
        public static void Inici()
        {
            bool fi;
            do
            {
                OpcionsMenu();
                fi = EligeManu();
            } while (!fi);

        }

        public static void OpcionsMenu()
        {
            Console.WriteLine("");
            Console.WriteLine("MENU");
            Console.WriteLine("");

            Console.WriteLine("1.Exercici1");
            Console.WriteLine("2.Exercici2");

            Console.WriteLine("5.SORTIR");

        }
        
        public static bool EligeManu()
        {
            string opcio = Convert.ToString(Console.ReadLine());
            Console.WriteLine("");

            switch (opcio)
            {
                case "1":
                    Exercici1();
                    break;
                case "2":
                    Exercici2();
                    //MetodesExercicis.Recursivity3x3();
                    break;
                case "3":
                    break;
                case "4":
                    break; 
                case "5":
                    return true;
                    
            }
            return false;
        }


        public static void Exercici1()
        {
            int[,] matriuTres = new int [3, 3];
            
            MetodesExercicis.GenMatriu3(matriuTres);
            
            Console.WriteLine("MATRIU 3 x 3 :");
            for (int x = 0; x < matriuTres.GetLength(1); x++)
            {
                for (int y = 0; y < matriuTres.GetLength(0); y++)
                {
                    Console.Write(matriuTres[x, y]+ " ");
                    
                }
                Console.WriteLine("\t");
            }
            
            Console.WriteLine("MATRIU 4 x 4 :");
            MetodesExercicis.GenMatriu4(matriuTres);
            
        }

        public static void Exercici2()
        {
            int[,] matriuTres = new int [3, 3];
            MetodesExercicis.GenMatriu3(matriuTres);
            Console.WriteLine("MATRIU 3 x 3 :");

            MetodesExercicis.Recursivity3x3(); 

        }
        
    }
}