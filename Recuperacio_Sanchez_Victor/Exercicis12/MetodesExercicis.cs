/*
Autor : Víctor Sánchez Íñiguez
Data : 10-06-2022
*/

using System;

namespace Exercicis12
{
    public class MetodesExercicis
    {

        /// <summary>
        /// Aquest mètode genera una matriu 3x3 amb números aleatoris // no acaben de sortir els negatius 
        /// </summary>
        /// <param name="matriuTres"></param>
        public static void GenMatriu3(int[,] matriuTres)
        {
            //int[] posiblenum = {-10, -9,-8,-7,-6,-5,-4,-3,-2,-1};             no escriu numeros negatius
            int i;
            int j = 0;
            Random aleatori = new Random();

            for (i = 0; i < matriuTres.GetLength(0); i++)
            {
                for (j = 0; j < matriuTres.GetLength(1); j++)
                {
                    if (matriuTres[i, j] < 10) // && (-10) < matriuTres[i,j])       no escriu numeros negatius
                    {
                        matriuTres[i, j] = aleatori.Next(matriuTres.Length);
                        //Console.WriteLine(matriuTres[i,j] + "\t");
                    }

                }
            }
        }

        /// <summary>
        /// Genera la matriu 4x4 a partir de la 3x3 encara que no he trobat solució a fer aquest exercici sencer tot amb for
        /// </summary>
        /// <param name="matriuTres"></param>
        public static void GenMatriu4(int[,] matriuTres)
        {
            int[,] matriuCuatre = new int[4, 4];
            int i;
            int j;

            for (i = 0; i < matriuTres.GetLength(0); i++)
            {
                for (j = 0; j < matriuTres.GetLength(1); j++)
                {
                    matriuCuatre[i, j] = matriuTres[i, j];
                }
            }

            matriuCuatre[0, 3] = matriuTres[0, 0] + matriuTres[0, 1] + matriuTres[0, 2];
            matriuCuatre[1, 3] = matriuTres[1, 0] + matriuTres[1, 1] + matriuTres[1, 2];
            matriuCuatre[2, 3] = matriuTres[2, 0] + matriuTres[2, 1] + matriuTres[2, 2];

            //Cuarta fila (suma total): 
            matriuCuatre[3, 0] = matriuCuatre[0, 3];
            matriuCuatre[3, 1] = matriuCuatre[1, 3];
            matriuCuatre[3, 2] = matriuCuatre[2, 3];
            matriuCuatre[3, 3] = matriuCuatre[0, 3] + matriuCuatre[1, 3] + matriuCuatre[2, 3];

            for (int x = 0; x < matriuCuatre.GetLength(0); x++)
            {
                for (int y = 0; y < matriuCuatre.GetLength(1); y++)
                {
                    Console.Write(matriuCuatre[x, y] + " ");
                }

                Console.WriteLine("\t");
            }
        }

        /// <summary>
        /// Mostrem la matriu 3x3 amb un mètode recursiu
        /// </summary>
        /// <returns></returns>
        public static string Recursivity3x3()
        {
            int[,] matriu3 = new int [3,3];
            int x = 0;
            int y = 0;
            GenMatriu3(matriu3);

            for (; x < matriu3.GetLength(1); x++)
            {
                for (; y < matriu3.GetLength(0); y++)
                {
                    string a = $"{matriu3[x, y]}"; 
                }
            }
            return Recursivity3x3();
        }

    }
}