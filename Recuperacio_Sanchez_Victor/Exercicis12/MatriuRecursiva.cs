using System;

namespace Exercicis12
{
    public class MatriuRecursiva
    {
        class Program
        {
            static int[,] Matriz = new int[3, 3];

            static int F1 = 0;
            static int C1 = 0;
            static int total = 0;
            
            static void Manue(string[] args)
            {
                Random random = new Random();

                for (int f = 0; f < 3; f++)
                {
                    for (int c = 0; c < 3; c++)
                    {
                        Matriz[f, c] = random.Next();
                    }
                }
                RecursividadFilas();

                Console.ReadLine();
            }
            
            private static void RecursividadFilas()
            {
                if (F1 < Matriz.Length / 3)
                {
                    if (C1 < Matriz.Length / 3)
                    {
                        Console.WriteLine($"Fila: {F1}, Column: {C1} - Valor: {Matriz[F1, C1]}");
                        C1++;
                        total++;
                        if (C1 == 3)
                        {
                            F1++;
                            C1 = 0;
                        }
                    }
                    RecursividadFilas();
                }
            }
        }
    }
}