﻿using System;

namespace Prova
{
    public class Program
    {
        
            //Aquest métode retorna un vector de números ordenats, començant per m i acabant amb n
            public static int[] NeedFunctionName(int m, int n)
            {
                var numero = new int[]{};
                int i;
                
                for (i=m ; i<n;i++)
                {
                    numero[i]=i;
                }

                return numero;
            }
            
            
            // S'espera el següent comportament
            //  0: No presentat
            //  1-4: Insuficient
            //  5-6: Suficient
            //  7-8: Notable
            //  9: Excelent
            //  10: Excelent + MH
            public static string Select(int i)
            {
                //TODO: Cahotic order. Make it easy, only one return.
                switch (i) 
                {
                    
                    case 1 or 2 or 3 or 4: return "Insuficient";
                    case 0:
                        return "No presentat";
                    case 10:
                        return "Excelent + MH";
                    case 5 or 6: return "Suficient";
                    case 7 :
                        return "Notable";
                    default:
                        return "No valid";
                }
            }

            //Retorna les posicions on apareix el caracter
            public int[] PositionsIntoString(string abc, char b)
            {
                int[] s = new int[] { };
                for (int i=0; i< abc.LastIndexOf(b);i++){
                    if (abc[i] == b)
                    {
                        if (s == null)
                        {
                            s = new int[1];
                            s[i] = b;
                        }
                        else
                        {
                            var mida = (s.Length)+1;
                            Array.Resize(ref s, mida );
                        }
                       
                    }
                }

                return s;
            }
            
            //Retorna la primera posició on apareix una lletra o un -1
            public int  FirstOccurrencePosition(string abc, char b)
            {
                for (var i=0; i<abc.Length; i++) {
                    if (abc[i] == b)
                    {
                        return -1;
                    }
                }
                
                return abc[0];
            }
        }
    
}

