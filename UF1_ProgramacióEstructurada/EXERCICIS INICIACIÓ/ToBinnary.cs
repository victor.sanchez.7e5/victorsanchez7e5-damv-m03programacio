/******************************************************************************

AUTHOR : Víctor S
Date : 11-11-2021
Description : Escriu un programa que llegeixi un nombre natural més petit que 256 i escrigui la seva representació en binari.

*******************************************************************************/
    
using System;
class ToBinnary {
  static void Main() {
    
    Console.Write("Escriu un nombre natural inferior a 256 :  ");
    int num = Convert.ToInt32 (Console.ReadLine());
    int i ; 
    
    while (num!=0) {
    
    i = num%2;
    num = num/2 ; 
    Console.Write("{0}", i);
    
    }
    
  }
}

