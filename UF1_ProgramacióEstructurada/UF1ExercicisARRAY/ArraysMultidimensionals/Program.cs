﻿using System;
using System.Diagnostics;
using System.Net;

namespace ArraysMultidimensionals
{
    internal class Program
    {
        public static void Mainn()
        {
            int key = Convert.ToInt32(Console.ReadLine());

            if (key == 1)
            {
                EnfonsaVaixells();

            }else if (key == 2)
            {
                MatrixBoxesOpenedCounter();
            }else if (key == 3)
            {
                RookMoves();
            }
        }

        public static void EnfonsaVaixells()
        {
            string[,] mapacoord = 
            {{"x", "x", "0", "0", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0"},
            {"0", "0", "0", "0", "0", "0", "0"} };
            int xcord;
            int ycord;
            int torns = 0; 

            while ( torns < 5 )
            {
                int comptavaixells = 0;

                Console.WriteLine("POSICIÓ EN X : ");
                xcord = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("POSICIÓ EN Y :");
                ycord = Convert.ToInt32(Console.ReadLine());
                
                if (mapacoord[ycord, xcord] == "x")
                {
                    Console.WriteLine("VAIXELL ENFONSAT");
                    mapacoord[ycord,xcord ] = "0"; 
                }
                else
                {
                    Console.WriteLine("AIGUA");
                }

                for (int x = 0; x < mapacoord.GetLength(0); x++)
                {
                    for (int y = 0; y < mapacoord.GetLength(1); y++)
                    {
                        if (mapacoord[x, y] == "x")
                        {
                            comptavaixells++; 
                        }
                    }
                }
                
                Console.WriteLine("Et queden "+comptavaixells+ " vaixells per enfonsar!");
                torns++;
                if (comptavaixells == 0)
                {
                    Console.WriteLine("Has Guanyat");
                    Console.Read();
                    torns = 10;

                }
            }
            
        }


        public static void MatrixBoxesOpenedCounter()
        {
            
        }
        
        /// <summary>
        ///
        /// </summary>
        static void RookMoves()
        {
            Console.Clear();
            string finish;
            string input;
            string[] data;
            do
            {
                string[,] board = new string[,]
                {
                    {"x", "x", "x", "x", "x", "x","x", "x"}, 
                    {"x", "x", "x", "x", "x", "x","x", "x"}, 
                    {"x", "x", "x", "x", "x", "x","x", "x"}, 
                    {"x", "x", "x", "x", "x", "x","x", "x"}, 
                    {"x", "x", "x", "x", "x", "x","x", "x"}, 
                    {"x", "x", "x", "x", "x", "x","x", "x"}, 
                    {"x", "x", "x", "x", "x", "x","x", "x"}, 
                    {"x", "x", "x", "x", "x", "x","x", "x"}
                };

                string rook = "♜";
                string rookShadow = "♖";
                
                Console.WriteLine("Tell me the position it will start the rook and I will show you to which position the rook can move");
                input = Console.ReadLine();
                if (input != null)
                {
                    input = input.ToUpper();
                    data = input.Split(' ');
                    int row = board.Length - int.Parse(data[0]);
                    data[1] = data[1].ToUpper();
                    int column;
                    switch (data[1])
                        {
                            case "A":
                                column = 0;
                                break;
                            case "B":
                                column = 1;
                                break;
                            case "C":
                                column = 2;
                                break;
                            case "D":
                                column = 3;
                                break;
                            case "E":
                                column = 4;
                                break;
                            case "F":
                                column = 5;
                                break;
                            case "G":
                                column = 6;
                                break;
                            case "H":
                                column = 7;
                                break;
                            default:
                                column = 0;
                                break;
                        }
                    
                    for (int i = 0; i < board.GetLength(0); i++)
                    {
                        board[row, i] = rookShadow;
                    }
                    for (int j = 0; j < board.GetLength(1); j++)
                    {
                        board[j, column] = rookShadow;
                    }

                    board[row, column] = rook;
                }

                for (int i = 0; i < board.GetLength(0); i++)
                {
                    Console.Write("| ");
                    for (int j = 0; j < board.GetLength(1); j++)
                    {
                        Console.Write(board[i, j] + " ");
                    }

                    Console.WriteLine("|");

                }

                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("If you want to exit, press N, if you want to rerun the program, press Y");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Blue;
                finish = Console.ReadLine();
                Console.ResetColor();
                do
                {
                    if (finish != null) finish = finish.ToUpper();

                    if (finish != "N" && finish != "Y")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Please, type Y or N.");
                        Console.ResetColor();
                        Console.ForegroundColor = ConsoleColor.Blue;
                        finish = Console.ReadLine();
                        Console.ResetColor();
                    }
                } while (finish != "N" && finish != "Y");
                
            } while (finish != "N");
            
        }
        
    }
}
