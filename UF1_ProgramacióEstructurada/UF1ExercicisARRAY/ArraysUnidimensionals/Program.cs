﻿using System;

namespace ArraysUnidimensionals
{
    internal class Program
    {
        public static void Mainn(string[] args)
        {
            //LetterinWord();
            Console.WriteLine("");
            //AddValuesToList();

            //Swap();
            PushButtonPadlockSimulator();
        }

        public static void LetterinWord()
        {
            string paraula = Console.ReadLine();
            int posició = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine(paraula[posició-1]);
        }

        public static void AddValuesToList()
        {
            float[] vector = new float[50];     // en un array de mida 50 l'última posició és la 49
            
            Console.Write("[");
            for (int i = 0; i < vector.Length-1; i++)
            {
                vector[i] = 0.0f;
                Console.Write(vector[i]+",");
            }
            Console.WriteLine(vector[49]+"]");
            
            vector[0] = 31.0f;
            vector[1] = 56.0f;
            vector[19] = 12.0f;
            vector[49] = 79.0f;
            
            Console.WriteLine("Canvis en el Vector Realitzats");
            
            Console.Write("[");
            for (int j = 0; j < vector.Length-1; j++)
            {
                Console.Write(vector[j]+",");
            }
            Console.Write(vector[49]+"]");

        }

        public static void Swap()
        {
            int[] vector = new int[4];
            int num = Convert.ToInt32(Console.ReadLine());
            int num1 = Convert.ToInt32(Console.ReadLine()); 
            int num2 = Convert.ToInt32(Console.ReadLine()); 
            int num3 = Convert.ToInt32(Console.ReadLine());

            vector[3] = num;
            vector[1] = num1;
            vector[2] = num2;
            vector[0] = num3; 
            
            Console.Write("[");
            for (int i = 0; i < vector.Length-1; i++)
            {
                Console.Write(vector[i]+",");
            }
            Console.Write(vector[3]+"]");
    
        }
        
        
        public static void PushButtonPadlockSimulator()
        {
            int[] candau = {1, 2, 3, 4, 5, 6, 7, 8};
            
             string   contra = Console.ReadLine();
            
            bool a; 
                
            for (int i = 0; i < candau.Length-1; i++)
            {
                if (candau[i] == contra[i])
                {
                    a = true; 
                    Console.WriteLine(a);
                }
            }
        }
        
        
        
    }
}