/******************************************************************************

Author : Víctor Sánchez 
Date : 1 Octubre 2021
Description : Llegeix un enter i imprimeix el doble del valor entrat.

*******************************************************************************/ 
using System;

class HelloWorld {

  static void Main() {

    Console.Write("Introdueix un numero:");

    double num = Convert.ToDouble(Console.ReadLine());

    Console.WriteLine(num*2);

  }

}