/******************************************************************************

Author : VC-ctor SC!nchez 
Date : 4 Octubre 2021
Description : 

*******************************************************************************/

using System;
class HelloWorld
{
  static void Main ()
  {
      
    Console.WriteLine ("Digues el dia del teu aniversari: ");
    
        int diaAniversari = Convert.ToInt32(Console.ReadLine());
        
    Console.WriteLine ("Digues el mes del teu aniversari: ");
    
        int mesAniversari = Convert.ToInt32(Console.ReadLine());
    
    Console.WriteLine ("Digues el dia actual: ");
    
        int diaActual = Convert.ToInt32(Console.ReadLine());
    
    Console.WriteLine ("Digues el mes actual: ");
    
        int mesActual = Convert.ToInt32(Console.ReadLine());
    
    Console.WriteLine((mesActual >= mesAniversari) || ((diaActual >= diaAniversari)&&(mesActual >=mesAniversari)));
    
  }
}
