/******************************************************************************
Farem un programa que demana repetidament a l'usuari un enter fins que entri el número 5.

*******************************************************************************/
using System;
class HowManyLines

{
  static void Main ()
  {
    Console.WriteLine ("Introdueix un text i escriu END per comptar el número de linies:");
    string texto = Console.ReadLine();
    int numlinies = 0;
    
    while (texto!="END")	
      {
    texto = Console.ReadLine();
    numlinies ++;

      }
         Console.Write("El numero de linies és :{0}",numlinies);

  }  
  
}
