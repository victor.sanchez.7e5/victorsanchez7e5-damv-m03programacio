/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
using System;
class NumberBetweenOneAndFive


{
  static void Main ()
  {
    
    int enter = 0;

    do {
        Console.WriteLine ("Introdueix un enter entre 1 i 5:");
        enter = Convert.ToInt32 (Console.ReadLine ());
        
        if (enter<1 || enter>5){
            Console.WriteLine ("No està entre 1 i 5:");
        }    
    }  while ((1>enter)|| (enter>5));
      
         Console.WriteLine("El número introduit: {0}",enter);

  }  
  
}
