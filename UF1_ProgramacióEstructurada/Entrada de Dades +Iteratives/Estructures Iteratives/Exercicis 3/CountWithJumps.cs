/******************************************************************************

Author: VíctorrrS
L'usuari introdueix dos valors enters, el final i el salt.
Escriu tots els numeros des de l'1 fins al final, amb una distància de salt
*******************************************************************************/

using System;
class CountWithJumps {
  static void Main() {
    Console.WriteLine("Introdueix un enter final: ");
    int a = Convert.ToInt32 (Console.ReadLine());
    Console.WriteLine("Introdueix enter de salt: ");
    int b = Convert.ToInt32 (Console.ReadLine());
    int i = 1; 
    
    if (a<b || b<1|| a<=b){
    Console.WriteLine ("Valors incorrectes");
    Console.WriteLine("Introdueix un enter final: ");
    a = Convert.ToInt32 (Console.ReadLine());
    Console.WriteLine("Introdueix enter de salt: ");
    b = Convert.ToInt32 (Console.ReadLine());
    }
    
    for (i = 1; i<a && a>=b ;i= i+b) {
        Console.Write ($"{i }");
    
    }
    
  }
}
