/******************************************************************************

L'usuari introdueix un enter (N) i es mostra per pantalla un compte enrere de N fins a 1.

*******************************************************************************/

using System;
class CountDown {
  static void Main() {
    Console.WriteLine("Introdueix un enter: ");
    int a = Convert.ToInt32 (Console.ReadLine());
    
    for (int i = a-1;(i<=a)&&(1<=i);i--) {
        Console.Write ($"{i}");
    }
    
  }
}


