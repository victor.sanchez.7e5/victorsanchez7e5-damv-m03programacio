/******************************************************************************

Imprimeix per pantalla tants punts com l'usuari hagi indicat

*******************************************************************************/
using System;
class DotLine {
  static void Main() {
    Console.WriteLine("Introdueix un enter: ");
    
    for (int m = 1;m<=9;m++) {
        for(int n=1; n<=9; n++){
                Console.WriteLine ("{0}*{1}={2}",m, n,(m*n));

        }
    }
    
  }
}


