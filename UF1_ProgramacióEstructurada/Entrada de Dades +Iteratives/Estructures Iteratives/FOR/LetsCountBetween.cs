/******************************************************************************

Imprimeix per pantalla tants punts com l'usuari hagi indicat

*******************************************************************************/

using System;
class DotLine {
  static void Main() {
    Console.WriteLine("Introdueix un enter: ");
    int a = Convert.ToInt32 (Console.ReadLine());
    Console.WriteLine("Introdueix un altre enter: ");
    int b = Convert.ToInt32 (Console.ReadLine());
    
    for (int i = a+1;(i>=a)&&(i<b);i++) {
        
        Console.Write ($"{i}");
    }
    
  }
}


