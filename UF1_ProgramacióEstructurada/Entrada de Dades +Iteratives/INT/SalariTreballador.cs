/******************************************************************************

Author : Víctor Sánchez 
Date : 1 Octubre 2021
Description : Una web d'habitatges de lloguer ens ha proposat una ampliació. 
Volen mostrar l'àrea de les habitacions en lloguer. 
Fes un programa que ens ajudi a calcular les dimensions d'una habitació. Llegeix 
l'amplada i la llargada en metres (enters) i mostra'n l'àrea.

*******************************************************************************/ 
using System;

class HelloWorld {

  static void Main() {

    Console.Write("Introdueix el numero d'hores treballades : ");

    int hores = Convert.ToInt32(Console.ReadLine());
    
    int hextra = hores-40 ;
    
    int total = 40*40 + 60*hextra ; 
    
    Console.Write("El salari del treballador es de :");
    
    Console.WriteLine(total);
    
  }

}

