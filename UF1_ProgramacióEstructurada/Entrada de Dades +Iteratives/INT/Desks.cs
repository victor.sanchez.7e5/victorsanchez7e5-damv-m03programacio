/******************************************************************************

Author : Víctor Sánchez 
Date : 1 Octubre 2021
Description : Una web d'habitatges de lloguer ens ha proposat una ampliació. 
Volen mostrar l'àrea de les habitacions en lloguer. 
Fes un programa que ens ajudi a calcular les dimensions d'una habitació. Llegeix 
l'amplada i la llargada en metres (enters) i mostra'n l'àrea.

*******************************************************************************/ 
using System;

class HelloWorld {

  static void Main() {

    Console.Write("Introdueix el numero de alumnes de la primera classe: ");

    int num = Convert.ToInt32(Console.ReadLine());
    
    Console.Write("Introdueix el numero d'alumnes de la segona classe:");
    
    int num2 = Convert.ToInt32(Console.ReadLine());
    
    Console.Write("Introdueix el numero d'alumnes de la tercera classe:");
    
    int num3 = Convert.ToInt32(Console.ReadLine());
    
    Console.Write("El número total de taules que necessitarem es : ");
    int total = (num + num2+ num3);
    Console.WriteLine( total/2+total%2);
    
  }

}
