/******************************************************************************

Author : Víctor Sánchez 
Date : 1 Octubre 2021
Description : Llegeix un enter i imprimeix el doble del valor entrat.

*******************************************************************************/ 
using System;

class HelloWorld {

  static void Main() {

    Console.Write("Introdueix un numero:");

    int num = Convert.ToInt32(Console.ReadLine());

    Console.WriteLine(num*2);

  }

}