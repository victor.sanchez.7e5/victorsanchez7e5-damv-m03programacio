/******************************************************************************

Author : Víctor Sánchez 
Date : 1 Octubre 2021
Description : Una web d'habitatges de lloguer ens ha proposat una ampliació. 
Volen mostrar l'àrea de les habitacions en lloguer. 
Fes un programa que ens ajudi a calcular les dimensions d'una habitació. Llegeix 
l'amplada i la llargada en metres (enters) i mostra'n l'àrea.

*******************************************************************************/ 
using System;

class HelloWorld {

  static void Main() {

    Console.Write("Introdueix un enter:");

    int num = Convert.ToInt32(Console.ReadLine());
    
    Console.Write("Introdueix un segon enter:");
    
    int num2 = Convert.ToInt32(Console.ReadLine());
    
    Console.Write("Introdueix un tercer enter:");
    
    int num3 = Convert.ToInt32(Console.ReadLine());
    
    Console.Write("Introdueix un cuart enter:");
    
    int num4 = Convert.ToInt32(Console.ReadLine());

    Console.WriteLine((num + num2) * (num3-num4));

  }

}