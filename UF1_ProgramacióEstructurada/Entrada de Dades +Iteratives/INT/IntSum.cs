/******************************************************************************

Author : Víctor Sánchez 
Date : 1 Octubre 2021
Description : Llegeix dos enters i imprimeix la suma dels dos.

*******************************************************************************/ 
using System;

class HelloWorld {

  static void Main() {

    Console.Write("Introdueix un numero:");

    int num = Convert.ToInt32(Console.ReadLine());
    
    Console.Write("Introdueix un altre numero:");
    
    int num2 = Convert.ToInt32(Console.ReadLine());

    Console.WriteLine(num + num2);

  }

}