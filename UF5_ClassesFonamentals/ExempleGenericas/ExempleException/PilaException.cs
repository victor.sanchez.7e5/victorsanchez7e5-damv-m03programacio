﻿using System;

namespace ExempleException
{
    public class PilaBuidaException : Exception
    {

        public PilaBuidaException(String missatge) : base("Problema:" + missatge)
        {
        }

    }
    public class Pila
    {
        class Node
        {
            public int Info;
            public Node Sig;
        }

        private Node _root;

        public Pila()
        {
            _root = null;
        }

        public void Inserir(int x)
        {
            Node nou;
            nou = new Node();
            nou.Info = x;
            if (_root == null)
            {
                nou.Sig = null;
                _root = nou;
            }
            else
            {
                nou.Sig = _root;
                _root = nou;
            }
        }

        public int Extraure()
        {
            if (_root != null)
            {
                int informacio = _root.Info;
                _root = _root.Sig;
                return informacio;
            }
            else
            {
                throw new PilaBuidaException("No hi ha mes elements a la pila");
            }
        }

        public bool Buida()
        {
            return _root == null;
        }
        public void Imprimir()
        {
            Node reco = _root;
            Console.WriteLine("Llistat de totss els elements de la pila.");
            while (reco != null)
            {
                Console.Write(reco.Info + "-");
                reco = reco.Sig;
            }
            Console.WriteLine();
        }
    }

    class Program
    {
        static void Main()
        {
            Pila pila1 = new Pila();
            pila1.Inserir(10);
            pila1.Inserir(40);
            pila1.Inserir(3);
            try
            {
                while (!pila1.Buida())
                    Console.WriteLine("Treiem de la pila:" + pila1.Extraure());
                Console.WriteLine("Treiem de la pila:" + pila1.Extraure());
            }
            catch (PilaBuidaException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
    }
}
