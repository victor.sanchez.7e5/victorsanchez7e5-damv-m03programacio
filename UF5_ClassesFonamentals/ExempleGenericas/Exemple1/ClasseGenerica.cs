﻿using System;

namespace Exemple1
{
    public class ClasseGenerica <T1, T2> {
    private T1 _x1;
    private T2 _x2;
    public ClasseGenerica (T1 p1, T2 p2)
    {
        _x1=p1;
        _x2=p2;
    }

    public override String ToString()
    {
        return _x1.ToString()+" - " +_x2.ToString();
    }
    public T1 GetX1() { return _x1; }
    public T2 GetX2() { return _x2; }

    }
}