﻿namespace Exemple1
{
    public class PilaEnters
        {
            private int[] vec = new int[5];
            private int _tope;

            public void Inserir(int x)
            {
                vec[_tope] = x;
                _tope++;
            }

            public int Treure()
            {
                _tope--;
                return vec[_tope];
            }
        }
}