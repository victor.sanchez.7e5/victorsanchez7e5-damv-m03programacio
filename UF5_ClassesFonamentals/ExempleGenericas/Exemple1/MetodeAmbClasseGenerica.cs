﻿using System;

namespace Exemple1
{
    public class MetodeAmbClasseGenerica
    {
        public static void  Metode<T1,T2>(ClasseGenerica<T1, T2> obj)
        {
            Console.WriteLine(obj);
        }
        public static void Main ()
        {
            
            ClasseGenerica <int, float> obj1 = new ClasseGenerica <int,float> (20, 42.45f);
            ClasseGenerica <Double, DateTime> obj2 = new ClasseGenerica <Double,DateTime> (4.32,new DateTime());
            Metode(obj1);
            Metode(obj2);
            PilaEnters pila1 = new PilaEnters();
            pila1.Inserir(20);
            pila1.Inserir(40);
            pila1.Inserir(17);
            Console.WriteLine(pila1.Treure()); 
            PilaStrings pila2 = new PilaStrings();
            pila2.Inserir("juan");
            pila2.Inserir("ana");
            pila2.Inserir("luis");
            Console.WriteLine(pila2.Treure());
            
        }
    }
}