﻿namespace Exemple1
{
    public class PilaStrings
    {
        private string[] vec = new string[5];
        private int _tope ;

        public void Inserir(string x)
        {
            vec[_tope] = x;
            _tope++;
        }

        public string Treure()
        {
            _tope--;
            return vec[_tope];
        }
    }
}