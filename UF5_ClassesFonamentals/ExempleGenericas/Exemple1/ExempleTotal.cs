﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Exemple1
{
    public class TaulaValorsNumerics <T>
    where T : unmanaged
    {
    private T[] t;

    public TaulaValorsNumerics (T[] obj) { t = obj; }

    public double Mitja()
    {
        return t.Sum;
    }
    
        public int Dimensio () { return t.Length; }

    private bool MateixaMitja<T> (TaulaValorsNumerics <T> obj) 
    {
        return Math.Abs(Mitja() - obj.Mitja()) <0.000000001 ;
    }

    private bool MateixaDimensio<T> (TaulaValorsNumerics <T> obj) 
    {
        return Dimensio() == obj.Dimensio();
    }

    public string toString()
    {
        string s="{";
        for (int i=0; i<t.Length; i++)
            if (s.Equals("{")) s = s + t[i];
            else s = s + ", " + t[i];
        s = s + "}";
        return s;
    }

    public static void Main ()
    {
        int[] ti= {1, 2, 5, 3, 4, 5, 5};
        double[] td = {1.1, 2.2, 3.3, 1.0, 4.4, 5.5, 6.6};
        string[] ts= { "Cad1", "Cad2" };
        TaulaValorsNumerics<int> tvn1= new TaulaValorsNumerics<int> (ti);
        TaulaValorsNumerics<double> tvn2 = new TaulaValorsNumerics<double> (td);
        // Les següents instruccions comentades no són compilables, per ser tipus genèric T restringit
// TaulaValorsNumerics tvn3 = new TaulaValorsNumerics (ts);
// TaulaValorsNumerics<String> tvn3 = new TaulaValorsNumerics<String> (ts);
        Console.WriteLine("tvn1: "+tvn1);
        Console.WriteLine("Mitja: "+tvn1.Mitja());
        Console.WriteLine("Dimensió: "+tvn1.Dimensio());
        Console.WriteLine("tvn2: "+tvn2);
        Console.WriteLine("Mitja: "+tvn2.Mitja());
        Console.WriteLine("Dimensió: "+tvn2.Dimensio());
        Console.WriteLine("tvn1 i tvn2 tenen la mateixa mitja? " + tvn1.MateixaMitja(tvn2));
        Console.WriteLine("tvn1 i tvn2 tenen la mateixa dimensió?" + tvn1.MateixaDimensio(tvn2));
    }
}
}