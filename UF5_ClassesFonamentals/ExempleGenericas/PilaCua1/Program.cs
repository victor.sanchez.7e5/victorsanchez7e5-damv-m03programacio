﻿using System;
using System.Collections.Generic;

namespace PilaCua1
{
    class Program
    {
 
            static void Main()
            {
                Stack<int> pila1 = new Stack<int>();
                Console.WriteLine("Inserim tres elements a la pila:10, 25 i 70");
                pila1.Push(10);
                pila1.Push(25);
                pila1.Push(70);
                Console.WriteLine("Quantitat d'elements de la pila:"+pila1.Count);
                Console.WriteLine("Treiem un element de la pila:" + pila1.Pop());
                Console.WriteLine("Quantitat d'elements de la pila:" + pila1.Count);

                Queue<string> cola1 = new Queue<string>();
                Console.WriteLine("Inserim tres elements a la cola:'ana', 'joan' i 'pere'");
                cola1.Enqueue("anna");
                cola1.Enqueue("joan");
                cola1.Enqueue("pere");
                Console.WriteLine("Quantitat d'elements a la cua:" + cola1.Count);
                Console.WriteLine("Treiem un element de la cua:" + cola1.Dequeue());
                Console.WriteLine("Quantitat d'elements en la cua:" + cola1.Count);
                Console.ReadKey();
            }
        }
    }