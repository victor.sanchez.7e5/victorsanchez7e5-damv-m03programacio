﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.ConstrainedExecution;
using P6_Interfícies_Excepcions.FiguraGeometrica;

namespace P6_Interfícies_Excepcions
{
    public class Program
    {
        public static void Main()
        {
            Console.ForegroundColor = ConsoleColor.White;
            
            Console.WriteLine("Taules de Figures Geomètriques : ");      //exemple
            // creem llista de figures
          List<FiguraGeometrica.FiguraGeometrica> listafiguras = new List<FiguraGeometrica.FiguraGeometrica>();
           
           //creem l'objecte tipus GrupFiguraGeometrica i creem els objectes que hi formen part
         GrupFiguraGeometricaViaTaula grupfg = new GrupFiguraGeometricaViaTaula(null);
           Cercle c1 = new Cercle(2, ConsoleColor.DarkMagenta, "cercle1", 2);
           Cercle c2 = new Cercle(4, ConsoleColor.Yellow, "cercle2 ", 5);
           Triangle t1 = new Triangle(1, ConsoleColor.Yellow, "triangle1 ", 5,2);
           Triangle t2 = new Triangle(8, ConsoleColor.Yellow, "triangle2 ", 5, 9 );
           Rectangel r1 = new Rectangel(10, ConsoleColor.Yellow, "rectangle1 ", 2,3);


           //afegim els objectes a grupfg  i els visualitzem : 

           grupfg.Afegir(c1);
           grupfg.Afegir(c2);
           grupfg.Afegir(t1);
           grupfg.Afegir(t2);
           grupfg.Afegir(r1);

           grupfg.Visualitzar();
           
           
           //Proves amb els diversos mètodes de GFG : 
           Console.ForegroundColor = ConsoleColor.White;
           
           
           Console.WriteLine("");
           Console.WriteLine("Cercant per codi la figura on codi : " + c1.GetCod());
           Console.WriteLine(grupfg.CercarPerCodi(2));
           
           
           grupfg.OrdenaPerCodi();
           Console.WriteLine();
           Console.WriteLine("Figures en ordre PER CODI>>>");
           grupfg.Visualitzar();

           Console.WriteLine("Figures en ordre PER AREA >>>");
           grupfg.OrdenaPerArea();
           grupfg.Visualitzar();

           Console.WriteLine(grupfg.CercaFigura(3));
           grupfg.Visualitzar();
           
           Console.WriteLine("Eliminem figura amb el codi : " + c1.GetCod());
           grupfg.Treure(3);
           
           Console.WriteLine("BUIDEM EL GRUP >>>");
           grupfg.BuidarGrup();
           grupfg.Visualitzar();
           
           grupfg.Treure(3);

           Console.ReadKey();
        }
        
    }
}