using System;
using System.Collections.Generic;
using P6_Interfícies_Excepcions.FiguraGeometrica;

namespace P6_Interfícies_Excepcions
{
    public class GrupFiguraGeometricaViaTaula : FiguraGeometrica.FiguraGeometrica
    {
    //private T[] taula;
    private List<FiguraGeometrica.FiguraGeometrica> fg; 

    public GrupFiguraGeometricaViaTaula(List<FiguraGeometrica.FiguraGeometrica> listafiguras)
    {
        fg = listafiguras; 
    }

    /// <summary>
    /// Mètodes Públics
    /// </summary>
    /// <param name="x"></param>
    public void Afegir(FiguraGeometrica.FiguraGeometrica x)
    {
        if (x != null) fg.Add(x);
        
            throw new FgException("Has d'afegir una figura amb dades !");
    }

    public object Treure(int codi)
    {   
        if (fg != null)
        {
            foreach (var figura in fg)
            {
                if (figura.GetCod() == codi)    // No hauria de ser amb un objecte genèric ?
                {
                    fg.Remove(figura);
                    return figura ;
                }
            }
        }

        if (fg == null) throw new FgException("La lista está vacía.");
        throw new FgException("No existe") ;
    }
    
    public object CercarPerCodi(int code)
    {
        // Ha d'haver un codi que fa referencia a un objecte 

        foreach (var figura in fg)
        {
            if (figura.GetCod() == code)    // No hauria de ser amb un objecte genèric ?
            {
                return figura ; 
            }
        }

        throw new FgException("Ponme uno que exista "); 
    }


    /// <summary>
    /// veure quina figura hi ha en una posició determinada
    /// </summary>
    public FiguraGeometrica.FiguraGeometrica CercaFigura(int pos)
    {
        return fg[pos]; 
    }

    public void BuidarGrup()
    {
        fg.Clear();
    }

    /// <summary>
    ///  mètode per ordenar la taula segons el codi de les figures.
    /// </summary>
    public void OrdenaPerCodi()
    {
        fg.Sort();      //amb la funció Sort de la LLISTA ens ordena els objectes per codi.
    }

    /// <summary>
    /// Mètode Ordenar que crida a la classe Algorismes amb el mètode de la bombolla per Comparar i ordenar
    /// </summary>
    public void Ordenar()
    {
        
    }
        
    /// <summary>
    ///  mètode per ordenar la taula segons l'àrea de les figures.
    /// </summary>
    public void OrdenaPerArea ()
    {
        fg.Sort(new ComparatorFiguraGeometricaArea());
    }

    //        La classe ha de sobreescriure els mètodes Equals() i GetHashCode() heretats de la classe Object. Ja hereden aquests mètodes de la classe Figura Geometrica
    

    /// <summary>
    /// Mostrem per pantalla les dades que conté la llista(grup) de figures geometriques :
    /// </summary>
    public void Visualitzar()
    {
        Console.WriteLine("-----------------------------------------");
        Console.WriteLine(" Els elements de Figura Geomètrica són : ");

        foreach (var figura in fg)
        {
            Console.WriteLine(figura);
        }
    }

    public override double CalculaArea()
    {
        throw new NotImplementedException();
    }
    }
}

