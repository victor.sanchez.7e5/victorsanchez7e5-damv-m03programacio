using System;

namespace P6_Interfícies_Excepcions.FiguraGeometrica
{
    public class Cercle: FiguraGeometrica
    {
        private double _radi;

        public Cercle()
        {
            _radi = 1;
        }

        public Cercle(int codi, ConsoleColor color, string nom, double radi) : base(codi, nom, color)
        {
            _radi = radi; 
        }

        public Cercle(Cercle c) : base( c )
        {
            _radi = c._radi;
        }
        
        public double GetRadi()
        {
            return _radi; 
        }

        public void SetRadi(double radi)
        {
            _radi = radi; 
            
        }
        
        public override string ToString()
        {
            Console.ForegroundColor = ConsoleColor.White;  
            if (Color != ConsoleColor.Black)
            {
                Console.ForegroundColor = Color;
            }
            return ""+Nom+ " amb codi "+Codi +" i radi: "+ _radi +" té una Longitud: " + CalculaLongitud() +" i Area: "+ CalculaArea(); 
        }

        public double CalculaLongitud()
        {
            return 2 * Math.PI * _radi; 
        }
        
        public override double CalculaArea()
        {
            //pi per radi al cuadrat : 
            return Math.PI * (_radi*_radi); 
        }
        
        public override bool Equals(Object obj)
        {
            FiguraGeometrica figura = obj as FiguraGeometrica;
            if (figura == null)
                return false;
            return base.Equals((FiguraGeometrica)obj);  // && _z == figura._z;
        }
    }
}

