using System;

namespace P6_Interfícies_Excepcions.FiguraGeometrica
{
    public class Rectangel : FiguraGeometrica
    {
        private double _base ;
        private double _altura;

        public Rectangel()
        {
            _base = 1;
            _altura = 1;
        }

        public Rectangel(int codi, ConsoleColor color, string nom, double b, double altura) : base(codi, nom, color)
        {
            _base = b;
            _altura = altura;
        }

        public Rectangel(Rectangel r) : base( r )
        {
            _base = r._base;
            _altura = r._altura;
        }

        
        public double GetBase()
        {
            return _base; 
        }

        public void SetBase(double vase)
        {
            _base = vase; 
        }

        public double GetAltura()
        {
            return _altura; 
        }

        public void SetAltura(double altura)
        {
            _altura = altura; 
        }
        
        public override string ToString()
        {
            Console.ForegroundColor = ConsoleColor.White;  
            if (Color != ConsoleColor.Black)
            {
                Console.ForegroundColor = Color;
            }
            return ""+Nom+" amb codi "+Codi+ " i Altura = " + _altura + " Base = " + _base + " /*/ Perimetro: "+ CalculaPerimetro()+" Area : "+CalculaArea();
        }
        public double CalculaPerimetro()
        {
            return _altura * 2 + _base * 2; 
        }
        public override double CalculaArea()
        {
            return _base * _altura;
        }
        
        public override bool Equals(Object obj)
        {
            FiguraGeometrica figura = obj as FiguraGeometrica;
            if (figura == null)
                return false;
            return base.Equals((FiguraGeometrica)obj); 
        }

    }
}

