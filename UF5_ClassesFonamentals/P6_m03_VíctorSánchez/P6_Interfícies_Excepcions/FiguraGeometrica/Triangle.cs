using System;

namespace P6_Interfícies_Excepcions.FiguraGeometrica
{
    public class Triangle : FiguraGeometrica
    {
        private double _base;
        private double _altura;

        public Triangle()
        {
            _base = 1;
            _altura = 1;
        }

        public Triangle(int codi, ConsoleColor color, string nom, double b, double altura) : base(codi, nom, color)
        {
            _base = b;
            _altura = altura;
        }

        public Triangle(Triangle t) : base( t )
        {
            _base = t._base;
            _altura = t._altura;
        }
        public double GetBase()
        {
            return _base; 
        }

        public void SetBase(double vase)
        {
            _base = vase; 
        }

        public double GetAltura()
        {
            return _altura; 
        }

        public void SetAltura(double altura)
        {
            _altura = altura; 
        }
        
        public override string ToString()
        {
            Console.ForegroundColor = ConsoleColor.White;  
            if (Color != ConsoleColor.Black)
            {
                Console.ForegroundColor = Color;
            }
            return  ""+Nom+" amb codi "+Codi+ " = " + _altura + "*" + _base + " perimetro: "+ CalculaPerimetro()+" area : "+CalculaArea();
        }
        public double CalculaPerimetro()
        {
            //suposant que el triangle és equilater 
            return _base *3; 
        }
        public override double CalculaArea()
        {
            return (_base * _altura)/2; 
        }
        
        public override bool Equals(Object obj)
        {
            FiguraGeometrica figura = obj as FiguraGeometrica;
            if (figura == null)
                return false;
            return base.Equals((FiguraGeometrica)obj); 
        }
        
    }
}

