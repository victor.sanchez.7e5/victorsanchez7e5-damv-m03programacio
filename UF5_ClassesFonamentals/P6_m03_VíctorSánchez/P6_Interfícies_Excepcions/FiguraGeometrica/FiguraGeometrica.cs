using System;
using System.Collections.Generic;

namespace P6_Interfícies_Excepcions.FiguraGeometrica
{
    public abstract class FiguraGeometrica : IComparable
    {
        // #codi = protected , +codi = public , -codi = private

        protected readonly int Codi;
        protected string Nom;
        protected ConsoleColor Color;

        public FiguraGeometrica()
        {
            Codi = 1;
            Nom = " a ";
            Color = ConsoleColor.Black;
        }

        public FiguraGeometrica(int codi, string nom, ConsoleColor color)
        {
            Codi = codi;
            Nom = nom;
            Color = color;
        }

        public FiguraGeometrica(FiguraGeometrica figura)
        {
            Codi = figura.Codi;
            Nom = figura.Nom;
            Color = figura.Color; 
        }
        
        public int GetCod()
        {
            return Codi; 
        }

        public string Getnom(string edwardElRectangle)
        {
            return Nom; 
        }

        public void Setnom(string nom)
        {
            Nom = nom;
        }

        public ConsoleColor GetColor()
        {
            return Color; 
        }

        public void Setcolor(ConsoleColor color)
        {
            Color = color; 
        }

        public abstract double CalculaArea();
        
        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            FiguraGeometrica fg = (FiguraGeometrica) obj;
            return (Codi == fg.Codi); //&& (Y == fg.Y);
        }
        
        public override int GetHashCode()
        {
            return (Codi << 2) ^22; 
        }

        public int CompareTo(object obj)
        {
            if (GetCod() == ((FiguraGeometrica)obj).GetCod()) return 0;
            if (GetCod() > ((FiguraGeometrica)obj).GetCod()) return 1;
            
            return -1;
        }
        

        internal class ComparatorFiguraGeometricaArea : IComparer<FiguraGeometrica>
        {
            public int Compare(FiguraGeometrica x, FiguraGeometrica y)
            {
                
                if (y != null && x != null && x.CalculaArea() > y.CalculaArea()) return 1;
                if (y != null && x != null && x.CalculaArea() < y.CalculaArea()) return -1;
                return 0;
                
            }
        }
        
        
    }
}


