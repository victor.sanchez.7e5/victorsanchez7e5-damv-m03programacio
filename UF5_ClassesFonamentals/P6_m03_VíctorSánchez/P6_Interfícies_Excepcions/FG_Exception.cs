using System;

namespace P6_Interfícies_Excepcions
{
    
    public class FgException : Exception
    {

        public FgException(String missatge) : base("Problema:" + missatge)
        {
        }
    }
    
    public class Pila
    {
        class Node
        {
            public int Info;
            public Node Sig;
        }

        private Node _root;

        public Pila()
        {
            _root = null;
        }

        public void Inserir(int x)
        {
            Node nou;
            nou = new Node();
            nou.Info = x;
            if (_root == null)
            {
                nou.Sig = null;
                _root = nou;
            }
            else
            {
                nou.Sig = _root;
                _root = nou;
            }
        }

        public int Extraure()
        {
            if (_root != null)
            {
                int informacio = _root.Info;
                _root = _root.Sig;
                return informacio;
            }
            else
            {
                throw new FgException("No hi ha mes elements a la pila");
            }
        }
        // AFEGIR EXCEPTION PER QUAN EL VALOR DE L'OBJECTE SIGUI NULL 

        public bool Buida()
        {
            return _root == null;
        }               
        public void Imprimir()
        {
            Node reco = _root;
            Console.WriteLine("Llistat de totss els elements de la pila.");
            while (reco != null)
            {
                Console.Write(reco.Info + "-");
                reco = reco.Sig;
            }
            Console.WriteLine();
        }
    }

    /*PROVES DEL PROGRAM
     
     class Program
    {
        static void Main()
        {
            Pila pila1 = new Pila();
            pila1.Inserir(10);
            pila1.Inserir(40);
            pila1.Inserir(3);
            try
            {
                while (!pila1.Buida())
                    Console.WriteLine("Treiem de la pila:" + pila1.Extraure());
                Console.WriteLine("Treiem de la pila:" + pila1.Extraure());
            }
            catch (FG_Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
    }          
                 *///PROVES DEL PROGRAM
}