﻿using System;
using System.IO;

namespace Principal
{
    
    /// <summary>
    /// Aquesta és la classe principal : 
    /// </summary>
    public class Pp6
    {
        
        public static void Exercici1()
      {
          string path = @"../../paraules/paraules.txt";

          string path_a = @"../../paraules/paraules-aaaaaa.txt"; 
          string path_e = @"../../paraules/paraules-eee.txt"; 
          string path_i = @"../../paraules/paraules-ioiii.txt"; 
          string path_o = @"../../paraules/paraules-ooooo.txt"; 
          string path_u = @"../../paraules/paraules-uhh.txt"; 

          
          StreamWriter sw = File.CreateText(path_a);
          StreamWriter se = File.CreateText(path_e);
          StreamWriter si = File.CreateText(path_i);
          StreamWriter so = File.CreateText(path_o);
          StreamWriter su = File.CreateText(path_u);

         
          using (StreamReader sr = File.OpenText(path))
          {
              // llegir arxiu
              string s;
              
              while ((s = sr.ReadLine()) != "xxxxx")
              {
                  foreach (var paraula in s)
                  {
                      if (s.StartsWith("a"))
                      {
                          File.CreateText(path_a);
                          sw.WriteLine(paraula);
                      }
                      else if (s.StartsWith("e"))
                      {
                          File.CreateText(path_e);
                          se.WriteLine(paraula);
                      }
                      else if (s.StartsWith("i"))
                      {
                          File.CreateText(path_i);
                          si.WriteLine(paraula);
                      }
                      else if (s.StartsWith("o"))
                      {
                          File.CreateText(path_o);
                          so.WriteLine(paraula);
                      }
                      else if (s.StartsWith("u"))
                      {
                          File.CreateText(path_u);
                          su.WriteLine(paraula);
                      }
                  }
                      
                      
              }
              
              Console.WriteLine("Paraules Carregades al documents");
          }
          
      }

        public static void Exercici2(string path, string term, string noupath)
        {
            StreamWriter sw = File.CreateText(noupath);
            
            using (StreamReader sr = File.OpenText(path))
            {
                // llegir arxiu
                string s;

                while ((s = sr.ReadLine()) != "xxxxx")
                {
                    if(s.EndsWith(term)) sw.WriteLine(s);
                }
            }
        }
    }
}


