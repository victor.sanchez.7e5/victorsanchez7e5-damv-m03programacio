﻿using System;
using System.Security.Permissions;

namespace Proves
{
    internal class Prova
    {
        public static void Main(string[] args)
        {
            Inici(); 
        }
        
        public static void Inici()
        {
            bool fi;
            do
            {
                OpcionsMenu();
                fi = EligeMenu();
            } while (!fi);

        }

        public static void OpcionsMenu()
        {
            Console.WriteLine("");
            Console.WriteLine("MENU");
            Console.WriteLine("");

            Console.WriteLine("1.Exercici1");
            Console.WriteLine("2.Exercici2");
            Console.WriteLine("3.");
            Console.WriteLine("5.Finalitza");
            
        }
        
        public static bool EligeMenu()
        {
            string opcio = Convert.ToString(Console.ReadLine());
            Console.WriteLine("");
            
            switch (opcio)
            {
                case "1":
                    Principal.Pp6.Exercici1();
                    break;
                case "2": 
                    Dades.Consola.DadesEx2();
                    break;
                case "3":
                    
                    break;
                case "5":
                    return true;
            }
            return false;
        }
        
    }
    
}