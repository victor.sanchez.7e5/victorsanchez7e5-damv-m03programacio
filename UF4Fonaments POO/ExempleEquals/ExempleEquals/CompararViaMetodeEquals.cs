﻿using System;

namespace ExempleEquals
{
    internal class CompararViaMetodeEquals
    {
        public static int GenericHash(params int[] campsHash)
        {
            int resultat = 17;
            foreach(int hash in campsHash)
            {
                resultat = 37 * resultat + hash;
            }
            return resultat;
        }
        
        // ara es comprova a proves 
    }
}