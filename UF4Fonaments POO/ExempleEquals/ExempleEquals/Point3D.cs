﻿using System;

namespace ExempleEquals
{
    public class Point3D : Point
    {
        readonly int _z;

        public Point3D(int x, int y, int z) : base(x, y)
        {
            _z = z;
        }

        public override bool Equals(Object obj)
        {
            Point3D pt3 = obj as Point3D;
            if (pt3 == null)
                return false;
            return base.Equals((Point)obj) && _z == pt3._z;
        }

        public override int GetHashCode()
        {
            return (base.GetHashCode() << 2) ^ _z;
        }

        public override String ToString()
        {
            return $"Point({X}, {Y}, {_z})";
        }
    }
}