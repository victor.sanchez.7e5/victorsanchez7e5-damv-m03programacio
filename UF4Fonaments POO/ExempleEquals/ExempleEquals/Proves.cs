using System;

namespace ExempleEquals
{
    public class Proves
    {
        public static void Main()
        {
            //Persona p1 = new Persona("","",0);
            /*Persona p2 = new Persona("","",0);
           if (p1.SetDni("12345678Z")==0)
           {
               return 0;
           }
        
           p1.SetNom("Big Boss");
            p1.SetEdat(33);
            Console.WriteLine("Visualització de persona p1:\n"+ p1);
            Console.WriteLine("El dni de p1 és " + p1.GetDni());
            Console.WriteLine("El nom de p1 és " + p1.GetNom());
            Console.WriteLine("L'edat de p1 és " + p1.GetEdat());
            Console.WriteLine("Visualització de persona p2:\n" + p2);
            return 1;*/
            Alumne[] t = new Alumne[6];
            //t[0] = new Alumne(p1,'?');
            t[1] = new Alumne("00000000", "Elden Ring", 12, 'b');
            t[2] = new Alumne("00000000", "Kratos", 1000);
            //t[3] = new Alumne(new Persona("00000000", "Arceus", 20), 'b');
            //t[4] = new Alumne(new Persona("00000000", "Link", 13));
            t[5] = new Alumne(t[3]);
            for (int i = 0; i < t.Length; i++) Console.WriteLine(t[i]);
            
            
            
            Point point2D = new Point(5, 5);
            Point3D point3Da = new Point3D(5, 5, 2);
            Point3D point3Db = new Point3D(5, 5, 2);
            Point point3Dc = new Point3D(5, 5, -1);
            string dni = "23333";
            string dni2 = "123";

            Console.WriteLine("{0} = {1}: {2}",
                point2D, point3Da, point2D.Equals(point3Da));
            Console.WriteLine("{0} = {1}: {2}",
                point2D, point3Db, point2D.Equals(point3Db));
            Console.WriteLine("{0} = {1}: {2}",
                point3Da, point3Db, point3Da.Equals(point3Db));
            Console.WriteLine("{0} = {1}: {2}",
                point3Da, point3Dc, point3Da.Equals(point3Dc));
            Console.WriteLine(dni.GetHashCode() + dni2.GetHashCode());
            Console.WriteLine(point2D);            
            
        }
    }
}