﻿using System;


namespace ExempleEquals
{
    public abstract class Persona
    {
        protected string Nom;
        protected string Dni;
        protected short Edat;
        
        public Persona (string sDni, string sNom, int nEdat)
        {
            Dni = sDni;
            Nom = sNom;
            if (nEdat>=0 && nEdat<=short.MaxValue)
                Edat = (short)nEdat;
        }

        public Persona(Persona p)
        {
            Nom = p.Nom;
            Edat = p.Edat;
            Dni = p.Dni;
        }
        
        
        int SetDni(string nouDni)
            // Retorna: 0 si s'ha pogut canviar el dni
            // 1 si el nou dni no és correcte - No s'efectua el canvi
        {
            // Aquí hi podria haver una rutina de verificació del dni
            // i actuar en conseqüència. Com que no la incorporem, // retornem sempre 0
            Dni = nouDni;
            return 0;
        }

        public void SetNom(string nouNom)
        {
            Nom = nouNom;
        }

        public short SetEdat(int novaEdat)
            // Retorna: 0 si s'ha pogut canviar l'edat
            // 1 : Error per passar una edat negativa
            // 2 : Error per passar una edat "enorme"
        {
            if (novaEdat < 0) return 1;
            if (novaEdat > short.MaxValue) return 2;
            Edat = (short) novaEdat;
            return 0;
        }


        public string GetDni()
        {
            return Dni;
        }

        public string GetNom()
        {
            return Nom;
        }

        internal short GetEdat()
        {
            return Edat;
        }

        public void Visualitzar()
        {
            string sortida = " Dni...........:" + Dni;
            sortida += "\n Nom...........:" + Nom;
            sortida += "\n Edat..........:" + Edat;
            Console.WriteLine(sortida);
        }
        public override string ToString()
        {
            string sortida = " Dni...........:" + Dni;
            sortida += "\n Nom...........:" + Nom;
            sortida += "\n Edat..........:" + Edat;
            return sortida;
        }
        public abstract Persona Clonar ();

        
    }

}

