﻿using System;

namespace ExempleEquals
{
    public class Point
    {
        protected readonly int X;
        protected readonly int Y;

        public Point() : this(0, 0)
        {
            
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Point p = (Point) obj; 
            return (X == p.X) && (Y == p.Y);
        }
        
        /*public override bool Equals(Object obj)
        {
            if (obj == this) return true;
            if (obj == null) return false;
            if (typeof(Point).IsInstanceOfType(obj))
            {
                Point aux = (Point)obj;
                return X.Equals(aux.X) && Y.Equals(aux.Y);
                                      //&& ... && campM.equals(C.campM);
            }
            return false;
        }*/
        public override int GetHashCode()
        {
            return (X << 2) ^ Y;
        }
        /*public override int GetHashCode()
        {
            return CompararViaMetodeEquals.GenericHash(X.GetHashCode(), Y.GetHashCode());
        }*/
        public override string ToString()
        {
            return $"Point({X}, {Y})";
        }
    }
}




