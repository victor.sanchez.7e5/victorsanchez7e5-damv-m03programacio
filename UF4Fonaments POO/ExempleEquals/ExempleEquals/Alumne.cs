﻿using System;

namespace ExempleEquals
{
    public class Alumne : Persona
    {
        private char _nivell;

        /** Valors vàlids:
        B = Batxillerat
        M = Cicle Fromatiu Mitjà
        S = Cicle Formatiu Superior
        ? = Desconegut
        */
        
        // Constructors ----------------------------------------------------
       public Alumne(string sDni, string sNom, int nEdat, char cNivell) : base(sDni, sNom, nEdat)   
        {
            _nivell = ValidarNivell(cNivell);
        }
        
        public Alumne(Persona p, char cNivell) : base(p)
        {
            _nivell = ValidarNivell(cNivell);
        }

        public Alumne(Persona p) : this(p, '?')
        {
        }

        public Alumne(string sDni, string sNom, int nEdat) : this(sDni, sNom, nEdat, '?')
        {
        }

        public Alumne(Alumne a) : this(a.Dni, a.Nom, a.Edat, a._nivell)
        {
        }
    

        //Mètodes públics --------------------------------------------------------------
        public void SetNivell (char nouNivell)
        {
            _nivell = ValidarNivell (nouNivell);
        }

        private char ValidarNivell (char nivell)
            /*Valida el "nivell" passat per paràmetre, de manera que retorna el valor vàlid i en majúscula. 
                Si no era     vàlid, retorna '?'. */
        {
            nivell = char.ToUpper(nivell);
            if (nivell!='B' && nivell!='S' && nivell!='M') nivell='?';
            return nivell;
        }

        public char GetNivell () { return _nivell; }

        public new void Visualitzar() 
        {
            base.Visualitzar();
            Console.WriteLine("Nivell........:" + _nivell);
        }

        public override string ToString()
        {
            string s = "Dni: " + Dni + " - Nom: " + Nom + " - Edat: " + Edat + " - Nivell: ";
            switch (_nivell)
            {
                case 'B': s = s + "Batxillerat"; break;
                case 'M': s = s + "Cicle F. Mitjà"; break;
                case 'S': s = s + "Cicle F. Superior"; break;
                default : s = s + "???"; break;
            }
            return s;
        }  
        
        public override Persona Clonar ()
        {
            var alumne = new Alumne (this);
            return alumne;
        }
    }
}