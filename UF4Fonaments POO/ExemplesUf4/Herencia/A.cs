﻿using System;

namespace Herencia
{
    public class A
    {
        protected int D=10;
        public void Xxx() { Console.WriteLine("D en A = " + D); }
        public void Xxx(int x)
        {
            char aux;
            if (typeof(C).IsInstanceOfType(this)) aux='C';
            else if (typeof(B).IsInstanceOfType(this)) aux='B';
            else aux='A';
            Console.WriteLine ("Sóc xxx d'A aplicat sobre un objecte de la classe " + aux );
        }
    }    
}
