﻿using System;

namespace Herencia
{
    public class B : A
    {
            protected new int D=20;
            public new void Xxx()
            {
                Console.WriteLine("D en B = " + D);
                base.Xxx();
            }
        
    }
}