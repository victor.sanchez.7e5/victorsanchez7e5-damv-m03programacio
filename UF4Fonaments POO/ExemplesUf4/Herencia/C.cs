﻿using System;

namespace Herencia
{
    public class C : B
    {
        new int D = 30;

        public new void Xxx()
        {
            Console.WriteLine("D en C = " + D);
            base.Xxx();
        }

        public void Visibilitat()
        {
            Console.WriteLine("Des del mètode \"visibilitat\" en C:");
            Console.WriteLine("D en C = " + D);
            Console.WriteLine("D en B = " + base.D);
        }
    }
}
