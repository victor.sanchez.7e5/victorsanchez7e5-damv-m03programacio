﻿using System;

namespace Herencia
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            A oa = new A();
            B ob = new B();
            C oc = new C();
            oc.Visibilitat(); // (1)
            Console.WriteLine("Crides al mètode Xxx() existent a les tres classes:");
            oa.Xxx(); // (2)
            ob.Xxx(); // (3)
            oc.Xxx(); // (4)
            Console.WriteLine("Crides al mètode Xxx(int x) existent a les tres classes:");
            oa.Xxx(0); // (5)
            ob.Xxx(0); // (6)
            oc.Xxx(0); // (7)
            
        }
        
    }
}