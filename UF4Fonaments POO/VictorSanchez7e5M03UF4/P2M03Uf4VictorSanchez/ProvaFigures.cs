using System;
using System.Drawing;
using System.Runtime.ConstrainedExecution;
using P2M03Uf4VictorSanchez;

namespace Practica1
{
    public class ProvaFigures
    {
        public static void Main(string[] args)
        {
            new Program().Inici();
        }
        
        public void DadesRectangle()
        {
            Console.WriteLine();
            Rectangel r1 = new Rectangel(); 
                    
            r1.SetBase(6);
            double novabase = r1.GetBase();
                
            r1.SetAltura(8);
            double novaaltura = r1.GetAltura();
            
            
            r1.SetCod(GetHashCode());
            r1.Setnom("Antoni el Rectangle");
            r1.Setcolor(ConsoleColor.Yellow);
            Console.ForegroundColor = r1.GetColor();
            Console.WriteLine(r1.ToString());
            Console.ForegroundColor = ConsoleColor.White; 
            
        }
        
        public void DadesTriangle()
        {
            
            Triangle t1 = new Triangle(); 
            t1.SetBase(3);      //Introduim base
            double basee = t1.GetBase();
                    
            t1.SetAltura(4);
            double altura = t1.GetAltura();
            
            t1.SetCod(GetHashCode());
            t1.Setnom("Josep el Triangle");
            t1.Setcolor(ConsoleColor.Cyan);
            Console.ForegroundColor = t1.GetColor();
            Console.WriteLine(t1.ToString());
            Console.ForegroundColor = ConsoleColor.White; 
        }

        public void DadesCercle()
        {
           
            Cercle c1 = new Cercle(); 
            c1.SetRadi(5);                  //Introduim radi
            double radi = c1.GetRadi(); 
            c1.SetCod(GetHashCode());
            c1.Setnom("Robinson el Cercle"); 
            
            c1.Setcolor(ConsoleColor.Magenta);
            Console.ForegroundColor = c1.GetColor();
            Console.WriteLine(c1.ToString());
            Console.ForegroundColor = ConsoleColor.White; 
            
        }
    }
}



