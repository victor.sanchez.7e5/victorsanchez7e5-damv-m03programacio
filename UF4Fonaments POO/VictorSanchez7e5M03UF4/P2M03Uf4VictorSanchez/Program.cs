﻿using System;
using System.Drawing;
using Practica1;
using Rectangle = System.Drawing.Rectangle;

namespace P2M03Uf4VictorSanchez
{
    internal class Program
    {
        public void Inici()
        {  
            bool fi;
            do
            {
                Menu();
                fi = EligeMenu (); 

            } while (!fi);
        }
        
        public static void Menu ()
        { 
            Console.WriteLine("");
            Console.WriteLine("fuera : per sortir del programa.");
            Console.WriteLine("Elige una de las opciones");
            Console.WriteLine("");
        }
        
        public bool EligeMenu()
        {
            string opcio = Console.ReadLine();
            Console.WriteLine("");
            ProvaFigures EntraEn = new ProvaFigures(); 
            
            switch (opcio.ToUpper().ToLower())
            {
                // he de passar els tostring com a objectes de la classe en comptes del Console.WriteLine
                case "rectangle":
                    EntraEn.DadesRectangle();
                    break;
            
                case "triangle":
                    EntraEn.DadesTriangle();
                    break;
            
                case "cercle" :
                    
                    EntraEn.DadesCercle();
                    break;
                case "fuera" :
                    return true;
            }
            return false;
        }

    }
}

