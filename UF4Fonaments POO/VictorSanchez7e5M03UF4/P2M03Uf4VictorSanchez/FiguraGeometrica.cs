using System;
using System.Drawing;

namespace Practica1
{
    public abstract class FiguraGeometrica
    {
        // #codi = protected , +codi = public , -codi = private

        protected int codi;
        protected string nom;
        protected ConsoleColor color; 
        
        public int GetCod()
        {
            return codi; 
        }

        public void SetCod(int _codi)
        {
            codi = _codi;
        }

        public string Getnom()
        {
            return nom; 
        }

        public void Setnom(string _nom)
        {
            nom = _nom;
        }

        public ConsoleColor GetColor()
        {
            return color; 
        }

        public void Setcolor(ConsoleColor _color)
        {
            color = _color; 
        }

        public abstract double CalculaArea();
        
        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            FiguraGeometrica fg = (FiguraGeometrica) obj;
            return (codi == fg.codi); //&& (Y == fg.Y);
        }
        
        public override int GetHashCode()
        {
            return (codi << 2) ^22; 
        }
    }
}


