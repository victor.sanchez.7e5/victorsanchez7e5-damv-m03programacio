using System.Drawing;

namespace Practica1
{
    public abstract class FiguraGeometrica
    {
        // #codi = protected , +codi = public , -codi = private
        
        protected int codi;
        protected string nom;
        public Color color; 
        
        public int GetCod()
        {
            return codi; 
        }

        public void SetCod(int _codi)
        {
            codi = _codi;
        }

        public string Getnom()
        {
            return nom; 
        }

        public void Setnom(string _nom)
        {
            nom = _nom;
        }

        public Color GetColor()
        {
            return color; 
        }

        public void Setcolor(Color _color)
        {
            color = _color; 
        }

        public abstract double CalculaArea(); 
    }
}