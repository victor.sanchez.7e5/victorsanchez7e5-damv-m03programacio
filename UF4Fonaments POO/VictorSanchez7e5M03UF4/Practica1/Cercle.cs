namespace Practica1
{
    public class Cercle
    {
        private double _radi;
        private double _pi = 3.14159; 

        public double GetRadi()
        {
            return _radi; 
        }

        public void SetRadi(double radi)
        {
            _radi = radi; 
            
        }
        
        public override string ToString()
        {
            return "" + _radi +" perimetro: " + CalculaLongitud() +" area : "+ CalculaArea(); 
        }

        public double CalculaLongitud()
        {
            return 2 * _pi * _radi; 
        }
        
        public double CalculaArea()
        {
            //pi per radi al cuadrat : 
            return _pi* (_radi*_radi); 
        }
    }
}

