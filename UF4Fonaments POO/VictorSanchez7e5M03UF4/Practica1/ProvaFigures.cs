using System;

namespace Practica1
{
    public class ProvaFigures
    {
        public void Inici()
        {  
            bool fi;
            do
            {
                Menu();
                fi = EligeMenu (); 

            } while (!fi);
        }
        
        public static void Menu ()
        { 
            Console.WriteLine("");
            Console.WriteLine("fuera : per sortir del programa.");
            Console.WriteLine("Elige una de las opciones");
            Console.WriteLine("");
        }
        
        public bool EligeMenu()
        {
            string opcio = Console.ReadLine();
            Console.WriteLine("");
            
            switch (opcio.ToUpper().ToLower())
            {
                // he de passar els tostring com a objectes de la classe en comptes del Console.WriteLine
                case "rectangle":
                    Rectangle r1 = new Rectangle();
                
                    r1.SetBase(6);
                    double novabase = r1.GetBase();
                
                    r1.SetAltura(8);
                    double novaaltura = r1.GetAltura();
                    Console.WriteLine(r1.ToString()); 
                    break;
            
                case "triangle":
                    Triangle t1 = new Triangle(); 
                    t1.SetBase(3);      //Introduim base
                    double basee = t1.GetBase();
                
                    t1.SetAltura(4);
                    double altura = t1.GetAltura();
                    Console.WriteLine(t1.ToString());
                    break;
            
                case "cercle" :
                    Cercle c1 = new Cercle(); 
                    c1.SetRadi(5);                  //Introduim radi
                    double radi = c1.GetRadi(); 
                
                    break;
                case "fuera" :
                    return true;
            }
            return false;
        }
        
    }
}



