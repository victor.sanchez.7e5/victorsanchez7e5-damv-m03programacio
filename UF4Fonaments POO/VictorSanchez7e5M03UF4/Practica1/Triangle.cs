namespace Practica1
{
    public class Triangle
    {
        private double _base;
        private double _altura;

        public double GetBase()
        {
            return _base; 
        }

        public void SetBase(double vase)
        {
            _base = vase; 
        }

        public double GetAltura()
        {
            return _altura; 
        }

        public void SetAltura(double altura)
        {
            _altura = altura; 
        }
        
        public override string ToString()
        {
            return "" + _altura + "*" + _base + " perimetro: "+ /*CalculaPerimetro()+*/" area : "+CalculaArea();
        }
        public double CalculaPerimetro()
        {
            //suposant que el triangle és equilater 
            return _base *3; 
        }
        public double CalculaArea()
        {
            return (_base * _altura)/2; 
        }
    }
}