namespace Practica1
{
    public class Rectangle
    {
        private double _base ;
        private double _altura;

        public double GetBase()
        {
            return _base; 
        }

        public void SetBase(double vase)
        {
            _base = vase; 
        }

        public double GetAltura()
        {
            return _altura; 
        }

        public void SetAltura(double altura)
        {
            _altura = altura; 
        }
        
        public override string ToString()
        {
            
            return "Altura = " + _altura + " Base = " + _base + " /*/ Perimetro: "+ CalculaPerimetro()+" Area : "+CalculaArea();
        }
        public double CalculaPerimetro()
        {
            return _altura * 2 + _base * 2; 
        }
        public double CalculaArea()
        {
            return _base * _altura; 
        }
    }
}