﻿using System;
using System.Collections.Generic;

namespace ExemplesInterface
{
    internal class Program
    {
        public static void Main(string[] args)
        {

            Gossa perraa = new Gossa("Gatita"); 
            Console.WriteLine(perraa.Descripcio());

            var gossetes = new List<Gossa>();
            gossetes.Add(new Gossa("Antonia"));
            gossetes.Add(new Gossa("Josefa"));
            gossetes.Add(new Gossa("Erminia"));

            foreach (var gosseta in gossetes)
            {
                Console.WriteLine(gosseta.Descripcio());
            }
        }
    }
}