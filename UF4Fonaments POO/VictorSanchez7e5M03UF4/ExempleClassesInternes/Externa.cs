using System;

namespace ExempleClassesInternes
{
    public class Externa
    {
        public void Metode1()
        {
            Console.WriteLine("Estic a la classe externa");
        }

        public class Interna
        {
            public void Metode2()
            {
                Console.WriteLine("Estic a la classe interna");
            }
        }
    }
}