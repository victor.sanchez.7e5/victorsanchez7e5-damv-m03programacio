﻿namespace ExempleClassesInternes
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Externa objecte1 = new Externa(); 
            objecte1.Metode1();
            Externa.Interna objecte2 = new Externa.Interna(); 
            objecte2.Metode2();
        }
    }
}
