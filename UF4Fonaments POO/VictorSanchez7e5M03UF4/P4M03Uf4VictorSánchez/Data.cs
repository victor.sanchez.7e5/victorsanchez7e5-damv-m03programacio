using System;
using System.Diagnostics.Tracing;
using P4M03Uf4VictorSánchez;

namespace P3M03Uf4VictorSanchez
{
    public class Data
    {
        
        private short dia;
        private short mes;
        private short any; 
        
        /// <summary>
        /// Mètode utilitzat per a escriure per pantalla la Data : 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            
            return dia + " - " + mes + " - "+any+" ";
        }

        /// <summary>
        /// Introduim els valors, dia, mes i any per teclat: 
        /// </summary>
        public void DataDefault(short diaa, short mess, short anyy)
        {
            dia = 1;
            mes = 1;
            any = 1980; 
        }
        
        public void SetData(short diaa, short mess , short anyy)
        {
            
            if (ComprovaDataa(diaa, mess, anyy))
            {
                dia = 1;
                mes = 1;
                any = 1980; 
            }
        }

        /// <summary>
        /// Quan fem servir aquest mètode ens retorna la Data: 
        /// </summary>
        /// <returns></returns>

        public short GetDia()
        {
            return dia;
        }

        public short GetMes()
        {
            return mes;
        }

        public short GetAny()
        {
            return any;
        }
        
        /// <summary>
        /// Comprovem que les dades introduides per parametre estiguin dintre dels límits establerts
        /// </summary>
        /// <param name="diaa"></param>
        /// <param name="mess"></param>
        /// <param name="anyy"></param>
        public bool ComprovaDataa(short diaa, short mess , short anyy)
        {
            bool datamal = false;
            if (IsLeapYear() && mess == 2)
            {
                //mes de febrer 28 dies, en leap year 29 !!!  
            }
            if (diaa > 31 || diaa < 1)
            {
                Console.WriteLine("Dia incorrecte");
                datamal = true; 
            }
            else
            {
                dia = diaa;
            }

            if (mess > 12|| mess < 1)
            {
                Console.WriteLine("Mes incorrecte");
                datamal = true; 
            }
            else
            {
                mes = mess; 
            }

            if (anyy < 1980)
            {
                Console.WriteLine("Any Incorrecte");
                datamal = true; 
            }
            else
            {
                any = anyy; 
            }

            return datamal; 
        }

        public void DataDefault()
        {
            dia = 1;
            mes = 1;
            any = 1980; 
        }
        
        
        /// <summary>
        /// COMPARA LES DATES : metode que compara els anys, mesos i dies de diferencia entre dues dates
        /// </summary>
        public int compdia (Data data , Data data2)
        {
            
            //diferencia de dias
            int diadiff; 
            if (data.GetDia() > data2.GetDia())
            {
                diadiff= data.GetDia() - data2.GetDia();
            }
            else
            {
                diadiff = data2.GetDia() - data.GetDia(); 
            }

            return diadiff; 
        }
        public int compmes (Data data , Data data2)
        {
            //diferencia de mes
            int mesdiff;
            if (data.GetMes() > data2.GetMes())
            {
                mesdiff= data.GetMes() - data2.GetMes();
            }
            else
            {
                mesdiff = data2.GetMes() - data.GetMes(); 
            }

            return mesdiff; 
        }
        public int company (Data data , Data data2)
        {
            //diferencia de dias
            int anydiff; 
            if (data.GetAny() > data2.GetAny())
            {
                anydiff= data.GetAny() - data2.GetAny();
            }
            else
            {
                anydiff = data2.GetAny() - data.GetAny(); 
            }

            return anydiff; 
        }
        
        /// <summary>
        /// Diferència de dies
        /// </summary>
        /// <returns></returns>
        /// 
        public bool IsLeapYear() 
        {
            if (any < 1980)
            {
                any = 1980; 
            }
            
            return any % 4 == 0 && (any % 100 != 0 || any % 400 == 0);
        }

        public void DaysDIFF()
        {
            
        }
        
    }
}