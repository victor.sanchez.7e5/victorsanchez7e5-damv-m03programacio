namespace P4M03Uf4VictorSánchez
{
    public class Algorismes : IOrdenable
    {
        
        public void Ordenar(IOrdenable[] obj, int nreElem)
        {
            
            bool ordenat=false;
            for (int i = 0; i < nreElem-1&&!ordenat; i++)
            {
                ordenat=true;
                for (int j = 0; j < nreElem-1-i; j++)
                {
                   if (obj[j].Comparar(obj[j+1]) == 1)
                    {
                        (obj[j], obj[j + 1]) = (obj[j + 1], obj[j]);
                        ordenat=false;
                    }
                }
            }
        }
        
        public int Comparar(IOrdenable x)
        {
            
            return 0; 
            
        }
    }
}