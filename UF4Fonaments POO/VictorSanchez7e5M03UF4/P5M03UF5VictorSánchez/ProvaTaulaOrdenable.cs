using System;
using Practica1;

namespace P5M03UF5VictorSánchez
{
    public class ProvaTaulaOrdenable
    {
        public static void Proves()
        {
            TOFG taula = new TOFG(50);
            taula.Afegir(GenRectangle());
            taula.Afegir(GenCercle());
            taula.Afegir(GenTriangle());
            //Generar un altre cercle   
            Cercle c2 = new Cercle(); 
            c2.Setnom("Snorlax el Circulo");
            c2.SetRadi(3);
            c2.SetCod(22);
            //Generar un altre rectangle
            Rectangel r2 = new Rectangel();
            r2.Setnom("Edward el Rectangle");
            r2.SetBase(10);
            r2.SetAltura(23); 
            r2.SetCod(125);
            taula.Afegir(c2);
            taula.Afegir(r2); 
            
            taula.Visualitzar();        // Mostrem la llista dels objectes creats ( Cercles, Triangles, Rectangles)
            
            taula.Ordenar();            // Apliquem l'ORDENAR
            

            Console.WriteLine();
            Console.WriteLine("TAULA ORDENADA : ");
            taula.Visualitzar();        // Mostrem la llista ordenada segons el criteri representat a FiguraGeometrica.Comparar

            
            Console.WriteLine("");
            Console.WriteLine("EXEMPLAR I EXTREURE");
            taula.ExemplarAt();         
            taula.Treure(); 
            taula.Visualitzar();
            
            Console.WriteLine("");

            Console.WriteLine("TAULA BUIDA : ");
            taula.Buidar();
            taula.Visualitzar();        // Visualitzem la taula completament buida 
            
        }

        /// <summary>
        /// Generem Figures
        /// </summary>
        /// <returns></returns>
        public static FiguraGeometrica GenRectangle()
        {
            Rectangel r1 = new Rectangel(); 
          
            r1.SetBase(6);
            r1.SetAltura(8);
            r1.SetCod(4);
            r1.Setnom("Antoni el Rectangle");
            r1.Setcolor(ConsoleColor.Yellow);
            
            return r1; 
        }
        public static FiguraGeometrica GenCercle()
        {
            
            Cercle c1 = new Cercle(); 
            c1.SetRadi(5);                  //Introduim radi
             
            c1.SetCod(7);
            c1.Setnom("Robinson el Cercle");
            
            return c1; 
        }

        public static FiguraGeometrica GenTriangle()
        {
            Triangle t = new Triangle(); 
            t.SetAltura(5);    
            t.SetBase(5);
            t.SetCod(7);
            t.Setnom("Alfredo el Triangle"); 
            t.Setcolor(ConsoleColor.Yellow);
            return t; 
        }
    }
}