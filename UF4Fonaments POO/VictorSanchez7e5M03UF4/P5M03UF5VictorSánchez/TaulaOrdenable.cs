using System;
using P4M03Uf4VictorSánchez;
using Practica1;

namespace P5M03UF5VictorSánchez
{
    public class TaulaOrdenable <T> where T : IOrdenable 
    {
        /// <summary>
        /// CONSTRUCTORS 
        /// </summary>
        private T[] taula;

        private int pos = 0;

        public TaulaOrdenable(T[] tabla)
        {
            taula = tabla;
        }
        
        /// <summary>
        /// Mètodes Públics
        /// </summary>
        /// <returns></returns>
        public int Capacitat()
        {
            return taula.Length;        // Retorna el màxim del array 
        }

        public int NrElements()
        {
            return pos; // Retorna la posició del Array
        }

        public int Afegir(T x)
        {

            if (pos == taula.Length)
            {
                return 2; 
            }
                                        //afegeix element a la taula  :
            if (x == null)
            {
                return 1;
            }
            
            taula[pos] = x;
            pos++;
            return 0;
        }
        
        /// <summary>
        /// Mètode que treu directament l'ultim element de la taula
        /// </summary>
        /// <returns></returns>
        public T Treure()
        {
            
            pos--;

            return taula[pos];
            
        }
        
        
        //ELS MÈTODES EXEMPLAR I EXTREURE NO COMPLEIXEN AMB LA SEVA FUNCIÓ :
        public T ExemplarAt()
        {
            if (pos<= 0 ||pos> taula.Length)
            {
                return default; 
            }
            return taula[pos];
        }
        
        /*
        public object ExtreureAt()
        {
            if (pos<= 0 ||pos> taula.Length)
            {
                return default; 
            }
            
            return taula[pos];
        }   */
        
        /// <summary>
        /// Mètode que buida completament la taula
        /// </summary>
        public void Buidar()
        {
            taula = new T[taula.Length];
            pos = 0;
        }
        
        /// <summary>
        /// Mostrem per pantalla les dades de capacitat i num d'elements per després mostrar element a element de la taula : 
        /// </summary>
        public void Visualitzar()
        {
            string impNrElements = "Capacitat : "+ Capacitat() +" Número d'elements: "+ NrElements();
            
            Console.WriteLine(impNrElements);
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine(" Els elements de Figura Geomètrica són : ");
            for (int i = 0; i < NrElements(); i++)
            {
                Console.Write("> ");
                Console.WriteLine(taula[i].ToString());
            }
        }

        /// <summary>
        /// Mètode Ordenar que crida a la classe Algorismes amb el mètode de la bombolla per Comparar i ordenar
        /// </summary>
        public void Ordenar()
        {
            
            Algorismes.Ordenar(taula,NrElements() );
            
        }
        
    }
}


