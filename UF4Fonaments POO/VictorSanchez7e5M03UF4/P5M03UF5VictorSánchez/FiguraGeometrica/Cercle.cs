using System;
using System.Drawing;
using System.Runtime.ConstrainedExecution;

namespace Practica1
{
    public class Cercle: FiguraGeometrica
    {
        private double _radi;
        private double _pi = 3.14159; 

        
        public double GetRadi()
        {
            return _radi; 
        }

        public void SetRadi(double radi)
        {
            _radi = radi; 
            
        }
        
        public override string ToString()
        {
            return ""+nom+ " amb codi "+codi +" i radi: "+ _radi +" té una Longitud: " + CalculaLongitud() +" i Area: "+ CalculaArea(); 
        }

        public double CalculaLongitud()
        {
            return 2 * _pi * _radi; 
        }
        
        public override double CalculaArea()
        {
            //pi per radi al cuadrat : 
            return _pi* (_radi*_radi); 
        }
        
        public override bool Equals(Object obj)
        {
            FiguraGeometrica figura = obj as FiguraGeometrica;
            if (figura == null)
                return false;
            return base.Equals((FiguraGeometrica)obj);  // && _z == figura._z;
        }
    }
}

