using System;
using P4M03Uf4VictorSánchez;

namespace Practica1
{
    public class Rectangel : FiguraGeometrica
    {
        private double _base ;
        private double _altura;

        public double GetBase()
        {
            return _base; 
        }

        public void SetBase(double vase)
        {
            _base = vase; 
        }

        public double GetAltura()
        {
            return _altura; 
        }

        public void SetAltura(double altura)
        {
            _altura = altura; 
        }
        
        public override string ToString()
        {

            return ""+nom+" amb codi "+codi+ " i Altura = " + _altura + " Base = " + _base + " /*/ Perimetro: "+ CalculaPerimetro()+" Area : "+CalculaArea();
        }
        public double CalculaPerimetro()
        {
            return _altura * 2 + _base * 2; 
        }
        public override double CalculaArea()
        {
            return _base * _altura;
        }
        
        public override bool Equals(Object obj)
        {
            FiguraGeometrica figura = obj as FiguraGeometrica;
            if (figura == null)
                return false;
            return base.Equals((FiguraGeometrica)obj); 
        }

    }
}

