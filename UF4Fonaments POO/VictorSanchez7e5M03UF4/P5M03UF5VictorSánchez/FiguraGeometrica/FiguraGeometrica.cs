using System;
using System.Drawing;
using P4M03Uf4VictorSánchez;

namespace Practica1
{
    public abstract class FiguraGeometrica : IOrdenable
    {
        // #codi = protected , +codi = public , -codi = private

        protected int codi;
        protected string nom;
        protected ConsoleColor color; 
        
        public int GetCod()
        {
            return codi; 
        }

        public void SetCod(int _codi)
        {
            codi = _codi;
        }

        public string Getnom(string edwardElRectangle)
        {
            return nom; 
        }

        public void Setnom(string _nom)
        {
            nom = _nom;
        }

        public ConsoleColor GetColor()
        {
            return color; 
        }

        public void Setcolor(ConsoleColor _color)
        {
            color = _color; 
        }

        public abstract double CalculaArea();
        
        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            FiguraGeometrica fg = (FiguraGeometrica) obj;
            return (codi == fg.codi); //&& (Y == fg.Y);
        }
        
        public override int GetHashCode()
        {
            return (codi << 2) ^22; 
        }

        /// <summary>
        /// Mètode que compara en els diferents nivells de figures per a la seva ordenació
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public  int Comparar(IOrdenable x)
        {
            
            if (typeof(Triangle).IsInstanceOfType(this) && typeof(Rectangel).IsInstanceOfType(x) || typeof(Cercle).IsInstanceOfType(x)|| typeof(Rectangel).IsInstanceOfType(this) && typeof(Cercle).IsInstanceOfType(x))
                return 1;
            var a = (FiguraGeometrica) x;
            if (GetType() != a.GetType()) return -1;
            if (CalculaArea() > a.CalculaArea()) return 1;
            if (CalculaArea() < a.CalculaArea()) return -1; 
            return 0; 
        }
    }
}


