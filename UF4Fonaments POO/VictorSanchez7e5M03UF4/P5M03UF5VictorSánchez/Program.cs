﻿// Author : Víctor Sánchez
// Date : 16-05-2022
// Description : 

using System;
using System.Runtime.ConstrainedExecution;
using Practica1;

namespace P5M03UF5VictorSánchez
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            ProvaTaulaOrdenable.Proves();
        }
    }
}