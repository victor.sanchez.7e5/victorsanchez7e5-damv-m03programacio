using Practica1;

namespace P5M03UF5VictorSánchez
{
    public class TOFG : TaulaOrdenable<FiguraGeometrica>
    {
        /// <summary>
        /// Constructor de la taula ordenable de figures geomètriques : 
        /// </summary>
        /// <param name="mida"></param>
        public TOFG(int mida) : base(new FiguraGeometrica[mida<=0 ? 10: mida])
        {
            
        }
        
    }
}