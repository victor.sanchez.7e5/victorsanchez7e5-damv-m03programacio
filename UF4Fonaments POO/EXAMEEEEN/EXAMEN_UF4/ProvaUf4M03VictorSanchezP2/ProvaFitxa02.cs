using System;

namespace ProvaUf4M03VictorSanchezP2
{
    public class ProvaFitxa02
    {
        public void Inici()
        {  
            bool fi;
            do
            {
                Menu();
                fi = EligeMenu (); 

            } while (!fi);
        }
        
        public static void Menu ()
        { 
            Console.WriteLine("");
            Console.WriteLine("fuera : per sortir del programa.");
            Console.WriteLine("Elige una de las opciones");
            Console.WriteLine("");
        }
        
        public bool EligeMenu()
        {
            string opcio = Console.ReadLine();
            Console.WriteLine("");
            
            switch (opcio.ToUpper().ToLower())
            {
                
                case "fuera" :
                    return true;
            }
            return false;
        }
    }
}
