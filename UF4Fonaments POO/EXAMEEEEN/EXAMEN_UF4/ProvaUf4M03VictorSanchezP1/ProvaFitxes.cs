using System;

namespace ProvaUf4M03VictorSanchezP1
{
    public class ProvaFitxes
    {
        public void Inici()
        {  
            bool fi;
            do
            {
                Menu();
                fi = EligeMenu (); 

            } while (!fi);
        }
        
        public static void Menu ()
        { 
            Console.WriteLine("");
            Console.WriteLine("Volum");
            Console.WriteLine("Revista");
            Console.WriteLine("Clona");
            Console.WriteLine("fuera : per sortir del programa.");
            Console.WriteLine("Elige una de las opciones");
            Console.WriteLine("");
        }
        
        public bool EligeMenu()
        {
            string opcio = Console.ReadLine();
            Console.WriteLine("");
            
            switch (opcio.ToUpper().ToLower())
            {
              
                case "volum" :
                    Volum vol = new Volum("Vaaa", "50 sombras de Peter","Jack Sparrow",69,1);
                    Console.WriteLine(GetHashCode());
                    Console.WriteLine(vol.ToString());
                    
                    break;
                
              
                case "revista":
                    Revista rev = new Revista("cusetes", "Lambo", 2011, 2); 
                    Console.WriteLine(rev.ToString());
                    Console.WriteLine(GetHashCode());

                    break;
                case "clona"  :
                    string reff = Console.ReadLine();
                    string titl = Console.ReadLine();
                    short any = 2016;
                    short nro = 3; 
                    Revista r = new Revista(reff, titl, any, nro);
                    Console.WriteLine(r.ToString());
                    Console.WriteLine(GetHashCode());
                    
                    r.Clonar();
                    Console.WriteLine("REVISTA CLONADA");
                    Console.WriteLine(r.ToString());
                    Console.WriteLine(GetHashCode());

                    if (Equals(r))
                    {
                        Console.WriteLine("Dues revistes diferents no poden tenir el mateix codi!");
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("No esta mal...");
                    }
                    break;
                
                
                case "fuera" :
                    return true;
            }
            return false;
        }
    }
}
