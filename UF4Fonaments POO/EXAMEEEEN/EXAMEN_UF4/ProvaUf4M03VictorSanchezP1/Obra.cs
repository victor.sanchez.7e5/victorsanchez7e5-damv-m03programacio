namespace ProvaUf4M03VictorSanchezP1
{
    public abstract class Obra : Fitxa
    {
        protected string autor;
        protected short nrePags;
        
        /// <summary>
        /// CONSTRUCTORS
        /// </summary>
        /// <param name="sreferencia"></param>
        /// <param name="sNom"></param>
        /// <param name="nEdat"></param>

        
        public Obra(string _referencia, string _titol, string _autor,short _nroPags) : base(_referencia, _titol)
        {
            autor = _autor;
            nrePags = _nroPags; 
        }

        public Obra (Obra o) : this(o.referencia, o.titol, o.autor, o.nrePags)
        {
            
        }

        /// <summary>
        /// METODES PÚBLICS
        /// </summary>
        /// <returns></returns>
        public string Getautor()
        {
            return autor; 
        }

        public void Setautor(string _autor)
        {
            autor = _autor;
        }   

        public short GetnrePags()
        {
            return nrePags; 
        }

        public void SetnrePags(short _nrePags)
        {
            nrePags = _nrePags;
        }   



    }
}