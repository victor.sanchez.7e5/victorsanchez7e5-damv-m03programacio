namespace ProvaUf4M03VictorSanchezP1
{
    public class Revista: Fitxa
    {
        private short any; 
        private short nro;

        /// <summary>
        /// CONSTRUCTORS
        /// </summary>
        /// <param name="_referencia"></param>
        /// <param name="_titol"></param>
        /// <param name="_any"></param>
        /// <param name="_nro"></param>
        public Revista(string _referencia, string _titol, short _any, short _nro) : base(_referencia, _titol)
        {
            any = _any;
            nro = _nro; 
        }

        public Revista(Revista r) : this(r.referencia, r.titol, r.any, r.nro)
        {
            
        }
        


        /// <summary>
        /// Mètodes públics 
        /// </summary>
        /// <returns></returns>
        public short Getnro()
        {
            return nro; 
        }

        public void Setnro(short _nro)
        {
            nro = _nro;
        }   
        public short Getany()
        {
            return any; 
        }

        public void Setany(short _any)
        {
            any = _any;
        } 
        public override string ToString()
        {

            return "La revista "+ titol +" amb codi de Referència: "+referencia+ "\n"+ "Any: " + any + " \nNúmero de revista: "+ nro;
        }
        public override Fitxa Clonar ()
        {
            var vRevista = new Revista(this);
            return vRevista;
        }
        public override int GetHashCode()
        {
            var i = 1;
            return (i++);
        }
    }
    
}