namespace ProvaUf4M03VictorSanchezP1
{
    public class Volum : Obra
    {
        
        private short nro;

        //CONSTRUCTORS
        
        public Volum(string _referencia, string _titol, string _autor, short _nroPags, short _nro) : base(_referencia, _titol, _autor, _nroPags)
        {
            
            nro = _nro; 
        }

        public Volum(Volum v) : this(v.referencia, v.titol,v.autor, v.nrePags, v.nro)
        {
            
        }
        /*
        public Volum(string _referencia, string _titol, string _autor, short _nrePags, short _nro) : base(_referencia, _titol, _autor, _nrePags)
        {
            nro = _nro;
        }

        public Volum(Obra o, short _nro) : base(o, _nro)
        {
            nro = _nro;
        }

        public Volum(Obra obra) : base(obra)
        {
        }

        public Volum(string _referencia, string _titol, string _autor, short _nrePags) : this(_referencia, _titol, _autor, _nrePags )
        {
            
        }

        public Volum(Volum v) : this(v.referencia, v.titol, v.autor, v.nrePags, v.nro)
        {
            
        }
        */
        
        /// <summary>
        /// MÈTODES PÚBLICS
        /// </summary>
        /// <returns></returns>
        public short Getnro()
        {
            return nro; 
        }

        public void Setnro(short _nro)
        {
            nro = _nro;
        }   
        
        public override string ToString()
        {

            return "A"; 
               // "L'obra " + titol +" amb codi de Referència: "+referencia+ "\n"+ "-Autor: " + autor + " \n-Número de Pàgines: "+ nrePags+"\n-Número de Volum: "+ nro;
        }
        
       /*public override Obra Clonar () //corregir constructors
        {
            var nouvolum = new Volum (this);
            return nouvolum;
        }   */


       public override Fitxa Clonar()
       {
           var volum = new Volum(this);
           return volum;
       }
    }
}