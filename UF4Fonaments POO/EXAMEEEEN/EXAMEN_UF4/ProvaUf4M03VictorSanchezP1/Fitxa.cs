using System;

namespace ProvaUf4M03VictorSanchezP1
{
    public abstract class Fitxa
    {
        protected string referencia;
        protected string titol;
        
        
        public Fitxa (string _referencia, string _titol)
        {
            referencia = _referencia;
            titol = _referencia;
            
        }
        
        public Fitxa(Fitxa f)
        {
            titol = f.titol;
            referencia = f.referencia;
        }
        
        

    /// <summary>
    /// METODES PÚBLICS
    /// </summary>
    /// <returns></returns>
        public string Getreferencia()
        {
            return referencia; 
        }

        public void Setreferencia(string _referencia)
        {
            referencia = _referencia;
        }   
        
        public string Gettitol()
        {
            return titol; 
        }

        public void Settitol(string _titol)
        {
            titol = _titol;
        }   
        public override bool Equals(Object obj)
        {
            Fitxa fitxa = obj as Fitxa;
            if (fitxa == null)
                return false;
            return base.Equals((Fitxa)obj); 
        }
        
        public abstract Fitxa Clonar ();
        
    }
}