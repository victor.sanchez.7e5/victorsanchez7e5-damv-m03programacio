﻿using System;
using ExercicisR;
using NUnit.Framework;

namespace TestRecursivity
{
    [TestFixture]
    public class Tests
    {
        /// <summary>
        /// Un valor a l'atzar donant com a enter el 9
        /// </summary>
        [Test]
        public void SerieDefinida1()
        {
            Assert.AreEqual(1560, Recursivity.SerieDefinida(9));
        }
        
        /// <summary>
        /// Probem un valor superior a 10 que hauria de donar donant-li com a enter l'11: 
        /// </summary>
        [Test]
        public void SerieDefinida2()
        {
            Assert.AreEqual(79647, Recursivity.SerieDefinida(11));
        }
        
        /// <summary>
        /// Comença a donar valors negatius a partir del rang de números del 20 al 40 aprox
        /// i quan torna a donar positius triga molta estona a donar la resposta
        /// </summary>
        [Test]
        public void SerieDefinidaBugeao()
        {
            //Assert.AreEqual(-1246082445, Recursivity.SerieDefinida(20));
            Assert.AreEqual(-1246082445, Recursivity.SerieDefinida(43));
            //Assert.Greater(0, Recursivity.SerieDefinida(40));
        }
        
    }
}