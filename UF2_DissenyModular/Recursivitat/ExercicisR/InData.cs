using System;

namespace ExercicisR
{
    public class InData
    {
        /// <summary>
        /// Mètode que demana enters i s'aplica a tots els mètodes que necessiten l'entrada d'un INT : 
        /// </summary>
        /// <returns></returns>
        public static int DemanaEnter()
        {
            int numInt = Convert.ToInt32(Console.ReadLine());
            return numInt;
        }

        
        
        public void DataComb()
        {
            int m = Convert.ToInt32(Console.ReadLine());
            int n = Convert.ToInt32(Console.ReadLine());
            int numcombi = 0; 
            Console.WriteLine(Recursivity.Combinacions(m,n,numcombi));

        }

        public void DataVectorSum()
        {
            /*un vector de n elements*/
            int n = Convert.ToInt32(Console.ReadLine()); 
            
            Random vector = new Random();
            char[] posiblecombinacio = {'A', 'B', 'C', 'D', 'E', 'F'}; 
            int rndsecret = vector.Next (posiblecombinacio.Length); 
        }
    }
}