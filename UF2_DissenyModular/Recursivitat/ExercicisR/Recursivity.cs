namespace ExercicisR
{
    public class Recursivity
    {
        public static int SerieDefinida(int i)
        {
            
            if (i == 1) return 0;
            if (i == 2) return 1; 
            
            return 3 * SerieDefinida(i-1) + 2* SerieDefinida(i-2); 
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dividend"></param>
        /// <param name="divisor"></param>
        /// <param name="quocient"></param>
        /// <returns></returns>
        
        public static int DivisionResta(int dividend, int divisor)
        {
            if (dividend < divisor) return 0;

            return 1+ DivisionResta(dividend-divisor, divisor); 
            
        }
            
        public static int  Combinacions(int m, int n, int numcombi)
        {
            
            if (n >= 1 && 0 <= m && 0 <= n)
            {
                
                if (m == 0 || m == n || n == 1)
                {
                    numcombi++;
                    return 1; 
                }
                else
                {
                    numcombi++; 
                    return Combinacions(n - 1, m,numcombi) + Combinacions(n - 1, m - 1,numcombi); 
                }
            }

            return numcombi; 
        }

        public static void VectorSum()
        {
            
        }
    }
}