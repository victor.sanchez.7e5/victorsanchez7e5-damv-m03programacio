using System;

namespace ExercicisR
{
    public class Menu
    {
        /*******Inici i Menu *******/
        public void Inici()
        {
            bool fi;
            do
            {
                ErManu();
                fi = EligeManu();
            } while (!fi);
            
        }
        
        void ErManu()
        {
            
            Console.Clear();    //netejar consola
            
            Console.WriteLine("Menú dels Exercicis de RECURSIVITAT: ");
            Console.WriteLine("--------------------------------------- ");
            
            Console.WriteLine("");
            
        }
        
        public static bool EligeManu()
        {
            string opcio = Convert.ToString(Console.ReadLine());
            Console.WriteLine("");
            
            ;
            
            switch (opcio)
            {
                case "1":
                    Exercici1();
                    break;
                case "2":
                    Exercici2(); 
                    break;
                
                case "3":
                    break;
                
                case "4":
                    break; 
                
                case "5" :
                    return true;
                    
            }
            return false;
        }

        /// <summary>
        /// Primer demanem la dada del número enter al mètode que tenim al document cs INDATA
        /// I després imprimim el mètode recursiu :
        /// </summary>
        public static void Exercici1()
        {
            Console.WriteLine("Escriu un número per començar la serie :");

            int n = InData.DemanaEnter(); 
            
            Console.WriteLine(Recursivity.SerieDefinida(n));

            Console.ReadKey();
        }

        public static void Exercici2()
        {
            Console.WriteLine("Escriu un número com a dividend : ");

            int dividend = InData.DemanaEnter();
            
            Console.WriteLine("Escriu un número com a divisor : ");
            int divisor = InData.DemanaEnter();
            
            
            Console.WriteLine(Recursivity.DivisionResta(dividend, divisor));
            Console.ReadKey();
        }
        
    }
}